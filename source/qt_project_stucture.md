# Project structure

* qt.cpp, qt.hpp: module entry point from VLC point of view

* maininterface: video compositor, window management, main context, QML entry point

* player: player view and model

* medialibrary: medialibrary models and views

* playlist: playqueue view and model

* network: network browse and service discovery (browse and discover tabs)

* dialogs: dialogs and error widgets (preferences, advanced effects,
  information panels, error dialogs, etc....)

* menus: contextual and application menus

* style: contains theme and style definition (colors, sizes), [Icons index](qt_font_icon)

* util: utilitarian classes

* widgets: standalone reusable widgets

* pixmaps: graphic assets

* Makefile.am: build file

* vlc.qrc, windows.qrc : list resources embed in the application: QML files, assets
