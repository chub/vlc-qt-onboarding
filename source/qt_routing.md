# Routing

To navigate between the different view, the application uses a routing system.

A route is set as a list of string (a path) and a dictionary to set the initial
properties of the view

The history is organized as a stack when you click a tab button or open the
player, a new path is pushed on the stack, the stack can be navigated back to go
to the previous view, views are popped from the stack, you can only go
backwards.

The `History` class will propose methods to manipulate the history stack:

* `History.push([path...], {properties...})` to push and go to a new page

* `History.previous()` to go back in the history

* `History.viewPath` gives the current history path

* `History.viewProp` gives the current history properties, this a "map-like"
  object, you can store values in this object, they will be passed as initial
  properties when navigating back in the history

* `History.match` and `History.extactMatch` allows to compare path to know
  whether a path is a subview or the same view as another view

## implementing a subview

To create a subview in QML, you need to use `Widgets.PageLoader`

```qml
//implemeting the ["mc", "music"] subview
Widgets.PageLoader {
    id: root

    //what are our subviews
    pageModel: [{
        //name of the subview
        name: "artists",
        //this is the default view to load
        default: true,
        //url of the subview
        url: "qrc:///medialibrary/MusicArtistsDisplay.qml"
    }, {
        name: "albums",
        //prevent loading of "album" if the condition is not met
        guard: function(prop) { return prop.foo !== undefined;  },
        url: "qrc:///medialibrary/MusicAlbumsDisplay.qml"
    }, {
        name: "genre",
        //sub pages can also be components
        component: browseGenreComponent,
    }]

    Component {
        id: browseGenreComponent
        MyComponent {
            onMyEvent: {
                //load the sibiling view "album"
                History.push(
                //path to load, `...root.pagePrefix` is the path  of the current page
                [...root.pagePrefix, "albums"],
                //initial properties of the view
                {
                    prop1: value1,
                    prop2: value2
                })
            }
        }
    }
}
```

see:

 * util/navigation_history.cpp

 * widget/qml/PageLoader.qml

 * widget/qml/StackViewExt.qml
