# Font icon

The UI uses some vector graphics that needs to adapt to the colors scheme used
by the application, as most graphics are monochrome we transforms our graphics
into a font, where each glyph correspond to a pictogram.

## Font definition

the picture that needs to be embedded in the font are defined in a file located at
`pixmaps/VLCIcons.json`, file should be self-explanatory (key/path), file uses the
JSON format, don't forget or add extra `,`

## Image restrictions

* SVG images should have a viewbox of "0 0 48 48"

* SVG should not have overlapping parts

* Fonts are monochrome, pictures will be treated as such

## Font generation

The font is generated using a custom script. The generation of this font is not
part of the usual build process and should be generated beforehand. This is done
using the script `makeIconFont.py` in the `pixmaps` folder like this:

```
./makeIconFont.py VLCIcons.json
```

this generates:

* VLCIcons.ttf : the actual font

* VLCIcons.json : the font index. This file must be placed in the `style` directory

in order to run this script you must have installed on your system fontforge and
its python3 bindings, python3-jinja2

on Ubuntu these are the following packages:

```
apt install python3-fontforge fontforge  python3-jinja2 python3-six
```

## Using icons in the application

We provide different classes that are already designed to work with icons. For
instance `IconButton` allows making a clickable button and `IconLabel` provides
some preset for visualizing an icon

```qml
Widgets.IconLabel {
    text: VLCIcons.history
    color: VLCStyle.colors.caption
}

Widgets.IconButton {
    font.pixelSize: VLCStyle.dp(20, VLCStyle.scale)
    text: VLCIcons.close
    onClicked: mainPlaylistController.stop()
}
```

The raw way to use the fonticon is by explicitly setting the font family and
the text of Text item.

```qml
Text {
    //icon to use
    text: VLCIcons.del
    //select the fontIcon
    font.family: VLCIcons.fontFamily
    //chose the color
    color: "pink"
}
```
