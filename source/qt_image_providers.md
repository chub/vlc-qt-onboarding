# Image providers

Some graphical resources are generated using
[QQuickImageProvider](https://doc.qt.io/qt-6/qquickimageprovider.html).

While this provide some flexibility regarding how image are generated there are
some limitation. The parameters of the image can only be defined by its URL,
this means that all parameters needs to be serialized in the URL, This is not
suitable for animations where parameters may often change and rapidly over
time.

## VLC access

Accessing artworks on disk or from the network should be made using specialized
access that will leverage VLC plugins, this ensures that files
discovered by the medialibrary can be opened by the interface as Qt default
image provider may not support the same protocols or may not have the right
credentials.

Serializing the url should be done using `VLCAccessImage.uri` helper

```
Image {
    source: VLCAccessImage.uri("scheme://url/of/the/resource")
}
```

## Shadows and effects

Some effects are generated as static image, this allows using shadows in
delegates that used list or grid without having a shader per item. Using these
effects should usually be done through specialized class that will expose and
handle the serialization of the different parameters such as `DefaultShadow` or
`DrowShadowImage`

Generating shadows is costly (CPU bound), you should not animate them.

## Colored SVG

`SVGColorImage` allow changing colors in an SVG artwork to match the theme of
the application.

This is working like a filter that substitute strings in the file, you should
avoid animating the colors, as this will be considered as a new media, will
re-read the file from disk and will generate a new image

It is usually preferred to use the font icon for monochrome artworks that need
to match the theme.

There are some predefined colors that can be used in artwork

| color   | template functions |
| ------- | -------- |
| #FF8800 | accent   |
| #FF00FF | color1   |
| #00FFFF | color2   |
