# Color context

Color is defined through context object, a context is attached to an object and
carries the type and state of the widget it is defined by:

  * the palette used
  * the type of widget (button, slider, spinbox, etc..)
  * the state of the object (enabled, hovered, pressed)

it will expose different sets of properties:

  * fg, bg are respectively colors sets for the foreground (text) and
    background. the set defines multiple colors:
      * primary: the main color to use
      * secondary: a complementary color, used for subtitles for instance
      * highlight: a color to use when the item is selected
      * positive/negative/neutral: to convey error or success

  * some accessory colors are provided aside the `bg` and `fg` set:
    * visualFocus: a color to mark that the item is currenly focused
    * border: a color for items borders
    * separator: a color to draw separation lines
    * indicator: to show indicators such as currently playing item
    * shadow: the color to show elevation
    * accent: the accent color (usually VLC's orange)

3 color palette exists:
 * Day and Night are hard coded colors theme provided by the application
 * System is a color theme that will try to match color defined by the system
   with the application, the system color theme is modular, it uses a plugin
   system that will retreive from plateform speficic library (highcontrast on
   windows or gtk colors for instance). A fallback using  Qt system palette is
   used if no native plugin exists.
