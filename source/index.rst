.. VLC Qt developer onbarding documentation master file, created by
   sphinx-quickstart on Thu Jun  9 10:08:24 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to VLC Qt developer onbarding's documentation!
======================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   qt_dev_tools
   qt_project_stucture
   qt_models
   qt_medialib
   qt_routing
   qt_keyboard_navigation
   qt_translation
   qt_persisting
   qt_font_icon
   qt_image_providers
   qt_colors
   qt_hig

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
