# Translation

VLC translation is base on [gettext](https://www.gnu.org/software/gettext/)

to mark a string for translation:

* on C++ side you should use `qtr` macro which is available in the `qt.hpp`,
  this will translate the enclosed string and return a QString

  ```
  addButton->setToolTip(qtr("My tooltip here"));
  ```

* For advanced usage, you can use vlc_gettext/vlc_ngettext functions directly

* For indirect translation (string inside a static list for instance), you
  should use the `N_` macro to mark strings for translation

  ```
  const char* myLabels[] = {
      N_("a label"),
      N_("another label")
  };

  for( size_t i = 0; i < ARRAY_SIZE(myLabels); i++)
      something->addEntry(qfu(myLabels[i]));
  ```

* on the QML side you need to use `qsTr("my string")`

* files that contain translated strings needs to be added to the
  `po/POTFILES.in`
