# Developer tools

## Debugging QML javascript

Qt Creator as a qml debuger which is helpful to debug js.

* To enable this, vlc must be compiled with `--enable-qt-qml-debug`

* to start the debugger vlc must be started with something like:
  `--qt-qmljsdebugger=port:4242,block`

* in qt creator "debug" > "start debugging" > "attach to QML port"

refer to [qt
documentation](https://doc.qt.io/qt-5/qtquick-debugging.html#starting-applications)
for more details

## QML caching

You can activate QML caching at compilation by passing the option
`--enable-qt-qml-cache` during configure. Qml files will be preprocessed at
compile time, this may catch some syntaxic errors

## Inspecting QML objects live

There is a tool named [GammaRay](https://github.com/KDAB/GammaRay/) which is
helpful to debug layout. It is dependent from the version of Qt, the best is
usually to compile it manually rather than using the version provided by your
distribution

## Testing with software rendering

```export QT_QUICK_BACKEND=software```

## Testing with older Qt version

It's usually a good practice to test the code against the minimal supported
version of Qt (6.2 at the time of writing). As the version on your system may be
different it, it may be necessary to install a Qt version manually, you can
either:

* install Qt using prebuilt version from the Qt Company. see
  https://doc.qt.io/qt-6/qt-online-installation.html for official documentation

  there are a few gotcha when using prebuilt version:

  * pkg-config file usually have the wrong prefix, you may need to fix them
  according to your installation

  ```bash
  /home/pierre/Qt/6.5.3/gcc_64/lib/pkgconfig
  sed -i -e "s#^prefix=.*#/home/pierre/Qt/6.5.3/gcc_64#" *.pc
  ```

  * early Qt 6.2 version don't provide .pc files this means that you'll need to
    either build vlc with meson or provide your own (you copy and adapt them
    them from a more recent version)

* build Qt yourself:

  ```bash
  # clone qt5 meta repo
  git clone https://github.com/qt/qt5
  cd qt5
  # checkout the desired version
  git checkout v6.2.8-lts-lgpl
  # init with the required modules
  ./init-repository --module-subset=qt5compat,qtbase,qtdeclarative,qtwayland,qtsvg,qtshadertools,qtimageformats
  # ensure that the dependencies files points to the open source repository
  find . -name "dependencies.yaml"  | xargs sed -i  -e "s/tqtc-//"
  # make a build tree
  mkdir build62
  cd build62
  # prefix is where Qt should be installed
  ../configure -prefix /home/pierre/Qt/6.2.8
  # build and install
  ninja
  ninja install
  ```

To configure and build VLC using this version of Qt you need to provide
different path at the time of the configure:

* when building with autotool, you need to provide the path for qt bin dir and
  the path of the pkg-config files

  ```bash
  ../configure PATH="/home/pierre/Qt/6.2.8/bin:$PATH"  PKG_CONFIG_PATH="/home/pierre/Qt/6.2.8/lib/pkgconfig/"
  ```

* when building with meson, you need the paths as
  ```
  PATH="/home/pierre/Qt/6.2.8/libexec/:$PATH"  meson -Dpkg_config_path=/home/pierre/Qt/6.2.8/lib/pkgconfig/
  ```

At runtime you may need to specify Qt library location

```
export LD_LIBRARY_PATH=/home/pierre/Qt/6.2.8/lib
./vlc
```

## Developer pitfalls

Some common issue made by newcomer (everyone actually ^_^):

* Beware of scaling issue, common sizes are defined in VLSStyle.qml and new ones
  should be defined using `dp()` for proper scaling

* Beware of translated strings, you should use `qsTr` in qml and `qtr` in C++,
  if this is in a new file, make sure that your file is listed in `po/POTFILES.in`

* Beware of theming colors, most colors should come from the `ColorContext`. You
  should test your code with light/dark/system theme.

* Beware of the keyboard navigation system, don't break it.

* When adding new files be sure that they are compiled by both autotools and
  meson. for QML files this means that the file should be listed in the `.qrc` and
  in the `Makefile.am`, for C++ they should be listed in the `Makefile.am` and in
  the `meson.build`

## QML coding convention

As usual, the convention is to follow the convention used in the file you edit,
tough following guides are worth reading to have best practices in mind.

* <https://doc.qt.io/qt-5/qml-codingconventions.html>

* <https://github.com/Furkanzmc/QML-Coding-Guide>
