# Persisting state

There are two ways to persist state and preference either with VLC variables or
using Qt QSettings system. The rule of thumb is that settings & preferences
should be stored using VLC variables (should we display CSD, should the
application resize to the video size, etc), whereas application state (window
position, is the playlist visible) should be stored in QSettings

* Most settings & preferences will be loaded by MainCtx within the
  `MainCtx::loadPrefs` procedure. Settings may be loaded manually for specific
  use case using the usual VLC `var_inheritXXXX` API on the `qt_intf_t` object.

* Most states will be loaded by MainCtx within the
  `MainCtx::loadFromSettingsImpl` and saved within `MainCtx` destructor
  the QSetting instance is store in the `qt_intf_t` object if there is a need to
  access outside MainCtx.
